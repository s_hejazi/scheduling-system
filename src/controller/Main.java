package controller;

import java.awt.EventQueue;

import javax.swing.JOptionPane;

import view.View;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Controller controller = new Controller();
					View frame = new View();
					frame.setSize(650, 690);
					frame.setVisible(true);
					frame.addController(controller);
					frame.setLocationRelativeTo(null);
					controller.addView(frame);
					frame.fillView();
					/***********************************************************************/
					//TODO REMOVE
					//TODO clear table
					//TODO open from file
					
					//controller.readInputFile();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	//	JOptionPane.showMessageDialog(null, "Please press Read excel sheet data from menu");
	}

}
