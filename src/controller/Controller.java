package controller;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import model.Bundle;
import model.Request;
import service.BundleDAO;
import service.RequestDAO;
import service.Scheduler;
import view.View;

public class Controller {
	View view;
	BundleDAO bundleService = new BundleDAO();
	RequestDAO requestService = new RequestDAO();

	Boolean check = false;
	public void addView(View view) {
		this.view = view;
	}

	public Boolean getCheck(){
		return check;
	}
	
	public void setCheck(Boolean check){
		this.check = check;
	}
	
	public Object[][] actionScheduling() {
		List<Bundle> bundleList = new ArrayList<Bundle>(); 
		bundleList = bundleService.getAllBundles();
		List<Bundle> assignedBundles;
		List<Request> backups;
		BigDecimal cost = BigDecimal.ZERO;
		
		List<Bundle> chosenAssignedBundles = new ArrayList<Bundle>();
		List<Request> chosenBackups = new ArrayList<Request>();
		BigDecimal min = BigDecimal.valueOf(Double.MAX_VALUE);

		Scheduler scheduler = new Scheduler();	
		scheduler.setNurseBundleMap(bundleList);
		List<Bundle> sortedBundles = scheduler.rankBundles(bundleList);
		int limit = sortedBundles.size()>15? 15: sortedBundles.size();
		for(int p=0; p<limit; p++){
		List<Bundle> newSortedBundles = new ArrayList <Bundle>();
		newSortedBundles.addAll(sortedBundles);
		assignedBundles = scheduler.assignBundles(newSortedBundles);
		backups = scheduler.getBackupRequests(assignedBundles);
		cost = calculateCost(assignedBundles, backups);
		//System.out.println(cost);
		if(cost.compareTo(min)<0){
			min = cost;
			chosenAssignedBundles = assignedBundles;
			chosenBackups = backups;
		}
		sortedBundles.remove(0);
		}
		
		int sum = 0;
		for (int i = 0; i < chosenAssignedBundles.size(); i++) {
			sum += chosenAssignedBundles.get(i).getPatientRequestList().size();
		}
		sum += chosenBackups.size();
		view.displayOverview(sum, min);
		List<Object[]> bundleData = new ArrayList<Object[]>();
		for (Bundle b : chosenAssignedBundles) {
			bundleData.add(new Object[] { b.getNurseID(), b.getRequestIdList(), b.getCost() });
		}
		for (int i = 0; i < chosenBackups.size(); i++) {
			bundleData.add(new Object[] { "B" + (i + 1), chosenBackups.get(i).getRequestID(), chosenBackups.get(i).getCost() });
		}
		setCheck(true);
		return bundleData.toArray(new Object[][] {});
	}


	public BigDecimal calculateCost(List <Bundle> assignedBundles, List<Request> backups) {
		BigDecimal total = BigDecimal.ZERO;

		for (Bundle b : assignedBundles) {
			total = total.add(b.getCost());
		}
		for (Request r : backups) {
			total = total.add(r.getCost());
		}
		return total;
	}
	
	public Object[][] actionDisplayNurseBundle() {
		List<Bundle> bundleList = bundleService.getAllBundles();
		List<Object[]> bundleValues = new ArrayList<Object[]>();

		if (bundleList != null)
			for (Bundle b : bundleList) {
				bundleValues.add(new Object[] { b.getNurseID(), b.getPatientRequestListForDisplay(), b.getCost() });

			}
		return bundleValues.toArray(new Object[][] {});
	}

	public Object[][] actionDisplayRequests() {
		List<Request> requestList = requestService.getRequestList();
		List<Object[]> requestInfo = new ArrayList<Object[]>();
		if (requestList != null)
			for (Request r : requestList) {
				requestInfo.add(new Object[] { r.getRequestID(), r.getCost() });
			}
		return requestInfo.toArray(new Object[][] {});
	}

	public void readInputFile(File myFile) {

		bundleService.dropTables();
		requestService.dropTables();


		List<BigDecimal> requestCost = new ArrayList<BigDecimal>();

		ArrayList<ArrayList<Integer>> listOfBundles = new ArrayList<ArrayList<Integer>>();
		List<BigDecimal> bundleCost = new ArrayList<BigDecimal>();
		Map<Integer, ArrayList<ArrayList<Integer>>> map1 = new LinkedHashMap<>();
		Map<Integer, List<BigDecimal>> map2 = new LinkedHashMap<>();

		try {
			if (myFile.exists()) {
				FileInputStream input = new FileInputStream(myFile);
				XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
				XSSFSheet mySheet = myWorkBook.getSheetAt(0);
				XSSFSheet mySheet1 = myWorkBook.getSheetAt(1);

				DataFormatter df = new DataFormatter();

				String column = "Standard Price";
				Integer columnNo = null;

				Row firstRow = mySheet.getRow(0);

				for (Cell cell : firstRow) {
					if (cell.getStringCellValue().equals(column)) {
						columnNo = cell.getColumnIndex();
					}
				}

				if (columnNo != null) {
					for (Row row : mySheet) {
						if (row.getRowNum() == 0) {
							continue; // just skip the row if row number is 0
						}
						Cell c = row.getCell(columnNo, MissingCellPolicy.CREATE_NULL_AS_BLANK);
					//	if (c.getCellType() != Cell.CELL_TYPE_BLANK) {
						if(c.getCellTypeEnum() != CellType.BLANK){
							String price = df.formatCellValue(c);
							StringBuilder sb = new StringBuilder(price);
							sb.deleteCharAt(0);
							String newPrice = sb.toString();
							requestCost.add(BigDecimal.valueOf(Long.parseLong(newPrice)));
							// requestService.addData(BigDecimal.valueOf(Long.parseLong(newPrice)));
						}
					}
				}
				requestService.addRequestList(requestCost);
				/*
				 * for (int i = 0; i < requestCost.size(); i++)
				 * requestService.addData( requestCost.get(i));
				 */

				List<Request> completeRequestList = requestService.getRequestList();

				Row secondRow1 = mySheet1.getRow(1);
				Integer costColumn = null;
				Integer bundleColumn = null;

				int nurseId = 0;
				BigDecimal Cost = null;
				for (Cell cell : secondRow1) {
					ArrayList<ArrayList<Integer>> bundlesList = new ArrayList<ArrayList<Integer>>();
					if (cell.getStringCellValue().equals("Visit Bundle")) {
						bundleColumn = cell.getColumnIndex();
						if (bundleColumn != null) {
							for (Row row : mySheet1) {
								if (row.getRowNum() < 2) {
									continue; // just skip the row if row number
												// is
												// less than 2
								}
								Cell c = row.getCell(bundleColumn, MissingCellPolicy.CREATE_NULL_AS_BLANK);	
								if (c.getCellTypeEnum() != CellType.BLANK){
									String test = c.getStringCellValue();
								if (test != null && !test.isEmpty()) {
									String test1 = test.replaceAll("\\s", "");

									String[] parts = test1.split(",");

									ArrayList<Integer> ints = new ArrayList<Integer>();
									for (int i = 0; i < parts.length; i++) {
										ints.add(Integer.parseInt(parts[i]));

									}
									listOfBundles.add(ints);
									bundlesList.add(ints);
								}

							}
							}
						}
						if (bundlesList != null && !bundlesList.isEmpty())
							map1.put(bundleColumn, bundlesList);
					}
					// }
					// System.out.println(map1);
					// for (Cell cell : secondRow1) {
					List<BigDecimal> costList = new ArrayList<BigDecimal>();
					if (cell.getStringCellValue().equals("Cost")) {
						costColumn = cell.getColumnIndex();
						if (costColumn != null) {
							for (Row row : mySheet1) {
								if (row.getRowNum() < 2) {
									continue; // just skip the rows if row
												// number is
												// less than 2
								}

								Cell c = row.getCell(costColumn, MissingCellPolicy.CREATE_NULL_AS_BLANK);
								if(c.getCellTypeEnum() != CellType.BLANK){
								double cost = c.getNumericCellValue();
								Cost = new BigDecimal(cost);
								}
								if (Cost.compareTo(BigDecimal.ZERO) != 0) {
									//System.out.println(costList.size());
									bundleCost.add(Cost);
									costList.add(Cost);
								}
							}
						}
						if (costList != null && !costList.isEmpty())
							map2.put(costColumn, costList);
					}

				}
				nurseId++;
				int count = 0;
				for (Integer key : map1.keySet()) {
					ArrayList<ArrayList<Integer>> bund = new ArrayList<ArrayList<Integer>>();
					bund = map1.get(key);
					List<BigDecimal> cost = new ArrayList<BigDecimal>();
					cost = map2.get(key + 1);
					for (int j = 0; j < bund.size(); j++) {
						ArrayList<Integer> req = bund.get(j);
						List<Request> rlist = new ArrayList<Request>();
						for (int k = 0; k < req.size(); k++) {
							rlist.add(completeRequestList.get(req.get(k) - 1));
						}
						bundleService.addData(cost.get(j), key - count, rlist);
					}
					count++;
				}

			} else {
				JOptionPane.showMessageDialog(null, "File not found");
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		JOptionPane.showMessageDialog(null, "Data read successfully.");

	}

	public long getNumberofRequests() {
		return requestService.getLastId();
	}



}
