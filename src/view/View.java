package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import controller.Controller;

public class View extends JFrame {
	Controller controller;
	JLabel label;
	JPanel panel, panel1;
	JTable table, table_1;
	
	public View() {
		getContentPane().setBackground(new Color(0, 130, 130));
		setTitle("Spectrum Scheduling");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 606, 604);
		getContentPane().setLayout(null);
		label = new JLabel();
		label.setFont(new Font("Lucida Grande", Font.BOLD, 14));
		label.setBounds(50, 86, 199, 28);
		label.setForeground(Color.black);
		getContentPane().add(label);
		table=new JTable();
		table_1 = new JTable();
	}

	public void fillView() {
		ImageIcon icon = new ImageIcon(new ImageIcon(getClass().getResource("/ico2.png")).getImage());// ).getScaledInstance(430, 290, Image.SCALE_DEFAULT));
		JLabel lblNewLabel_1 = new JLabel(icon);
		lblNewLabel_1.setBackground(new Color(176, 196, 222));
		lblNewLabel_1.setForeground(new Color(0, 0, 102));
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.BOLD, 19));

		// lblNewLabel_1.setIcon(icon);
		lblNewLabel_1.setBounds(89, 115, 434, 289);
		getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Welcome to scheduling system");
		lblNewLabel_2.setForeground(Color.BLACK);
		lblNewLabel_2.setFont(new Font("Lucida Grande", Font.BOLD, 21));
		lblNewLabel_2.setBounds(143, 416, 350, 28);
		getContentPane().add(lblNewLabel_2);
		
		panel = new JPanel();
		panel.setBackground(new Color(0, 130, 130));
		panel.setLayout(new FlowLayout());
		panel.setBounds(26, 119, 566, 340);
		getContentPane().add(panel);
		
		panel1 = new JPanel();
		panel1.setBackground(new Color(0, 130, 130));
		panel1.setLayout(new FlowLayout());
		panel1.setBounds(26, 511, 550, 340);
		getContentPane().add(panel1);
		
		
		JButton btnNewButton = new JButton("Schedule");
		btnNewButton.setForeground(new Color(0, 0, 0));
		btnNewButton.setBackground(new Color(140, 200, 200));
		btnNewButton.setBounds(26, 6, 174, 68);
		btnNewButton.setFont(new Font("Lucida Grande", Font.BOLD, 12));
		getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1.setVisible(false);
				lblNewLabel_2.setVisible(false);
				Object columnNames1[] = {"Nurse ID", "Assigned Bundles", "Cost (CAD)"};
				createTable(columnNames1, controller.actionScheduling());
				label.setText("Schedule");
				//displaySchedule();
			}

		});

		JButton btnNewButton_1 = new JButton("View submitted bundles");
		btnNewButton_1.setBackground(new Color(140, 200, 200));
		btnNewButton_1.setBounds(212, 6, 174, 68);
		btnNewButton_1.setFont(new Font("Lucida Grande", Font.BOLD, 12));
		getContentPane().add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1.setVisible(false);
				lblNewLabel_2.setVisible(false);
				Object columnNames[] = { "Nurse ID", "Submitted bundle", "Cost (CAD)" };
				createTable(columnNames, controller.actionDisplayNurseBundle());
				if(controller.getCheck()== false){
					displayOverview(0, BigDecimal.ZERO);
				}
				label.setText("Bundles");
				//displayNurseBundle();
			}

		});

		JButton btnNewButton_2 = new JButton("View requests");
		btnNewButton_2.setBackground(new Color(140, 200, 200));
		btnNewButton_2.setBounds(398, 6, 174, 68);
		btnNewButton_2.setFont(new Font("Lucida Grande", Font.BOLD, 12));
		getContentPane().add(btnNewButton_2);
		btnNewButton_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_1.setVisible(false);
				lblNewLabel_2.setVisible(false);
				Object columnNames3[] = { "Request ID", "Price" };
				createTable(columnNames3, controller.actionDisplayRequests());
				if(controller.getCheck()== false){
					displayOverview(0, BigDecimal.ZERO);
				}
				label.setText("Requests");
			//	displayRequests();
			}
		});

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

				JMenuItem mnOperation = new JMenuItem(new AbstractAction("File Browser") {
			 
 			public void actionPerformed(ActionEvent e) {

				JFileChooser fileChooser = new JFileChooser();
				int value = fileChooser.showOpenDialog(null);
				if(value == JFileChooser.APPROVE_OPTION){
					File file = fileChooser.getSelectedFile();
					if(file.getName().toLowerCase().endsWith(".xlsx")){
					controller.readInputFile(file);
					clearTable(table);
					clearTable(table_1);
					controller.setCheck(false);
				}
					else
						JOptionPane.showMessageDialog(null, "Chosen file is invalid");
					
				}
 			}
 
 		});
		mnFile.add(mnOperation);

		JMenuItem mntmQuit = new JMenuItem(new AbstractAction("Exit"){

			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
			
		});
		mnFile.add(mntmQuit);
		
		JMenu mntmHelp = new JMenu("Help");
		menuBar.add(mntmHelp);

		JMenuItem mntmView = new JMenuItem(new AbstractAction("View user manual"){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Desktop.isDesktopSupported()) {
				    try {
				        File myFile = new File("src/usermanual.pdf");
				        Desktop.getDesktop().open(myFile);
				    } catch (IOException ex) {
				        // no application registered for PDFs
				    }
				}
				
			}
			
		});
		mntmHelp.add(mntmView);
	}

	public void clearTable(JTable table){
		if(table.getRowCount()!=0){
			for (int i = 0; i < table.getRowCount(); i++){
			      for(int j = 0; j < table.getColumnCount(); j++) {
			          table.setValueAt("", i, j);
			      }
			   }
			}
	}
	public void createTable(Object[] columns, Object[][] rows){
		panel.removeAll();
		panel.repaint();
		panel.revalidate();
	    DefaultTableModel model = new DefaultTableModel(rows, columns);
		table = new JTable(model);
		table.setGridColor(new Color(0, 0, 139));
		table.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		table.setRowMargin(5);
		table.setRowHeight(20);
		table.setBackground(new Color(255, 250, 240));
		table.setEnabled(false);
	//	table.setBounds(26, 117, 567, 327);
		table.setPreferredScrollableViewportSize(new Dimension(560,330));
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBackground(new Color(255, 255, 255));
		scrollPane.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
	//	scrollPane.setBounds(26, 119, 566, 340);
		scrollPane.setPreferredSize(new Dimension(560,330));
		panel.add(scrollPane);
	}
	public void displayOverview(int assignedRequestsNum, BigDecimal sum){
		panel1.removeAll();
		panel1.repaint();
		panel1.revalidate();
		panel1.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblNewLabel = new JLabel("Overview");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setFont(new Font("Lucida Grande", Font.BOLD, 14));
	//	lblNewLabel.setBounds(26, 471, 199, 28);
		lblNewLabel.setPreferredSize(new Dimension(200,28));
	//	lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		panel1.add(lblNewLabel);
	//	getContentPane().add(lblNewLabel);
		Object columnNames2[] = { "Number of received requests", "Number of assigned requests", "Total cost (CAD)" };
		Object rowData2[][] = { { controller.getNumberofRequests(), assignedRequestsNum, sum } };
		table_1 = new JTable(rowData2, columnNames2);
		table_1.setShowHorizontalLines(false);
		table_1.setFillsViewportHeight(true);
		table_1.setGridColor(new Color(0, 0, 139));
		table_1.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		table_1.setRowMargin(5);
		table_1.setRowHeight(20);
		table_1.setBackground(new Color(255, 250, 240));
		table_1.setEnabled(false);
		//table_1.setBounds(26, 511, 550, 340);
		table_1.setPreferredScrollableViewportSize(new Dimension(550,340));
		JScrollPane scrollPane2 = new JScrollPane(table_1);
		scrollPane2.setBackground(new Color(255, 255, 255));
		scrollPane2.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		//scrollPane2.setBounds(26, 511, 550, 44);
		scrollPane2.setPreferredSize(new Dimension(550,44));
		//getContentPane().add(scrollPane2);	
		panel1.add(scrollPane2);
	}
	public void addController(Controller controller) {
		this.controller = controller;
	}

	public void displaySchedule() {
       /* Object columnNames1[] = {"Nurse ID", "Assigned Bundles", "Cost (CAD)"};
        DefaultTableModel model = new DefaultTableModel(controller.actionScheduling(), columnNames1);
		createTable(model);
        table = new JTable(model);
		table.setGridColor(new Color(0, 0, 139));
		table.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		table.setRowMargin(5);
		table.setRowHeight(20);
		table.setBackground(new Color(255, 250, 240));
		table.setBounds(26, 117, 547, 327);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBackground(new Color(255, 255, 255));
		scrollPane.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		scrollPane.setBounds(26, 119, 546, 340);
		getContentPane().add(scrollPane);*/
	}
	public void displayNurseBundle() {
	/*	Object columnNames[] = { "Nurse ID", "Submitted bundle", "Cost (CAD)" };
		DefaultTableModel model = new DefaultTableModel(controller.actionDisplayNurseBundle(), columnNames);
	    createTable(model);
		table.setModel(model);
		table.setGridColor(new Color(0, 0, 139));
		table.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		table.setRowMargin(5);
		table.setRowHeight(20);
		table.setBackground(new Color(255, 250, 240));
		table.setBounds(26, 117, 547, 327);
		JScrollPane scrollPane2 = new JScrollPane(table);
		scrollPane2.setBackground(new Color(255, 255, 255));
		scrollPane2.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		scrollPane2.setBounds(26, 119, 546, 340);
		getContentPane().add(scrollPane2);*/
	}

	public void displayRequests() {
	/*	String columnNames3[] = { "Request ID", "Price" };
		DefaultTableModel model1 = new DefaultTableModel(controller.actionDisplayRequests(), columnNames3);
	    createTable(model1);
		table.setModel(model1);
		JTable table1 = new JTable(model1);
		table1.setGridColor(new Color(0, 0, 139));
		table1.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		table1.setRowMargin(5);
		table1.setRowHeight(20);
		table1.setBackground(new Color(255, 250, 240));
		table1.setBounds(26, 117, 547, 327);
		JScrollPane scrollPane1 = new JScrollPane();//table1);
		scrollPane1.setViewportView(table1);
		scrollPane1.setBackground(new Color(255, 255, 255));
		scrollPane1.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		scrollPane1.setBounds(26, 119, 546, 340);
		getContentPane().add(scrollPane1);*/
	}
}
