package service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import model.Request;

public class RequestDAO {

	long last_Id = 0;

	public void addRequest(Request r) {
		Session session = SessionFactoryUtil.getInstance().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(r);
			session.getTransaction().commit();

		} catch (RuntimeException e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			// session.flush();
			session.close();
		}

	}

	public List<Request> getRequestList() {
		List<Request> requestList = new ArrayList<Request>();
		Session session = SessionFactoryUtil.getInstance().openSession();
		try {
			Transaction tx = null;
			tx = session.beginTransaction();
			Query query = session.createQuery("from Request");
			requestList = query.list();
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			// session.flush();
			session.close();
		}
		return requestList;
	}

	public void addData(BigDecimal cost) {
		Request r = new Request();
		r.setCost(cost);
		addRequest(r);
	}

	public void addRequestList(List<BigDecimal> costList) {
		Session session = SessionFactoryUtil.getInstance().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			for (int i = 0; i< costList.size(); i++) {
				Request r = new Request();
				r.setCost(costList.get(i));
				r.setRequestID(i+1);
				session.save(r);
			}
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			// session.flush();
			session.close();
		}
	}

	public long getLastId() {
		Session session = SessionFactoryUtil.getInstance().openSession();
		last_Id = ((Long) session.createQuery("select count(*) from Request").uniqueResult()).intValue();
		session.close();
		return last_Id;
	}


	public Set<Integer> getAllStoredRequestId(){
		Set<Integer> requestIdList = new HashSet<Integer>();
		for (Request R: getRequestList()){
			requestIdList.add(R.getRequestID());
		}
		return requestIdList;
	}
	public Request getRequestById(int id) {
		Session session = SessionFactoryUtil.getInstance().openSession();
		Transaction tx = null;
		Request r = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from Request where id=:id");
			query.setInteger("id", id);
			r = (Request) query.uniqueResult();
			session.getTransaction().commit();

		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return r;
	}
	
	public List<Request> getRequestListById(Set<Integer> requestIdList){
		ArrayList <Request> requests = new ArrayList<Request>();
		for (int r : requestIdList) {
			requests.add(getRequestById(r));
		}
		return requests;
	}
	
	public void dropTables(){
		Session session = SessionFactoryUtil.getInstance().openSession();
        try {
			Transaction tx = null;
			tx = session.beginTransaction();
            session.createQuery("delete from Request").executeUpdate();;
            tx.commit();
        }
        catch (RuntimeException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        }
        finally {
			session.close();
		}
	}
}
