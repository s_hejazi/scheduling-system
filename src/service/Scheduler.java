package service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import model.Bundle;
import model.Request;

public class Scheduler {

	
	
	Map<Integer, ArrayList<ArrayList<Integer>>> nurse_request_map;
	Map<Bundle, Integer> Bundle_rank_map;
	

	public Scheduler() {
	
		nurse_request_map = new HashMap<Integer, ArrayList<ArrayList<Integer>>>();	
		Bundle_rank_map = new HashMap<Bundle, Integer>();


	}

	public void setNurseBundleMap(List<Bundle> bundleList){
		for (Bundle b : bundleList) {
			if (nurse_request_map.containsKey(b.getNurseID())) {
				nurse_request_map.get(b.getNurseID()).add(b.getRequestIdList());
			} else {
				ArrayList<ArrayList<Integer>> temp = new ArrayList<ArrayList<Integer>>();
				temp.add(b.getRequestIdList());
				nurse_request_map.put(b.getNurseID(), temp);
			}
		}
	}
	public ArrayList<Bundle> rankBundles(List<Bundle> bundleList) {
		List<Bundle> sorted_bundle_list = new ArrayList<Bundle>();
		Map<Bundle, Integer> bundle_intersection = new HashMap<Bundle, Integer>();
		Map<Bundle, Integer> bundle_intersection2 = new HashMap<Bundle, Integer>();
		Map<Bundle, BigDecimal> bundle_cost_difference = new HashMap<Bundle, BigDecimal>();

		for (Bundle b : bundleList) {
			BigDecimal totalCost = BigDecimal.ZERO;
			int intersection = 0;
			int intersection2 = 0;
			for (Request r : b.getPatientRequestList()) {
				totalCost = totalCost.add(r.getCost());
			}
			for (int i : nurse_request_map.keySet()) {
				if (i != b.getNurseID()) {
					for (int j = 0; j < nurse_request_map.get(i).size(); j++) {
						ArrayList<Integer> temp = b.getRequestIdList();
						temp.retainAll(nurse_request_map.get(i).get(j));
						if (temp.size() > 0) {
							intersection++;
							intersection2 += temp.size();
						}
					}
				}
			}
			totalCost = totalCost.subtract(b.getCost());
			bundle_intersection.put(b, intersection);
			bundle_intersection2.put(b, intersection2);
			bundle_cost_difference.put(b, totalCost);
		}

		List <Integer> intersectionList = new ArrayList<>();
		intersectionList.addAll(bundle_intersection.values());
		List <Integer> intersectionSet = intersectionList.stream().distinct().collect(Collectors.toList());
		Collections.sort(intersectionSet);
		
		for(Bundle b: bundleList){
			int i = bundle_intersection.get(b);
			bundle_intersection.put(b, intersectionSet.indexOf(i));
		}
		List <Integer> intersectionList2 = new ArrayList<>();
		intersectionList2.addAll(bundle_intersection2.values());
		List <Integer> intersectionSet2 = intersectionList2.stream().distinct().collect(Collectors.toList());
		Collections.sort(intersectionSet2);

		for(Bundle b: bundleList){
			int i = bundle_intersection2.get(b);
			bundle_intersection2.put(b, intersectionSet2.indexOf(i));
		}
		/*List<Bundle> sorted_bundle_intersection = bundle_intersection.entrySet().stream()
				.sorted(Entry.comparingByValue()).map(e -> e.getKey()).collect(Collectors.toList());
*/
		 
		List<Bundle> sorted_bundle_cost_difference = bundle_cost_difference.entrySet().stream()
				.sorted(Entry.comparingByValue(Collections.reverseOrder())).map(e -> e.getKey())
				.collect(Collectors.toList());
/*		  for(Bundle b : sorted_bundle_cost_difference)
		  System.out.println(b.getBundleID()+ " has cost"+ bundle_cost_difference.get(b));*/
/*		 for(Bundle b : bundle_intersection.keySet())
		   System.out.println(b.getBundleID() +" has int "+bundle_intersection.get(b)+ " "+ bundle_intersection2.get(b));
		 */
		/*List<Bundle> sorted_intersection2 = bundle_intersection2.entrySet().stream()
				.sorted(Entry.comparingByValue(Collections.reverseOrder())).map(e -> e.getKey())
				.collect(Collectors.toList());
*/

		
		for (int i = 0; i < sorted_bundle_cost_difference.size(); i++) {
/*			int pos = sorted_bundle_intersection.indexOf(sorted_bundle_cost_difference.get(i));
			pos+=
			 sorted_intersection2.indexOf(sorted_bundle_cost_difference.get(i));*/
			int pos = bundle_intersection.get(sorted_bundle_cost_difference.get(i));///bundle_intersection.size();
			pos+= bundle_intersection2.get(sorted_bundle_cost_difference.get(i));///bundle_intersection2.size();
			pos += i;
			Bundle_rank_map.put(sorted_bundle_cost_difference.get(i), pos);
		}

		/*for(Bundle b: Bundle_rank_map.keySet())
			System.out.println(b.getBundleID()+" has pos "+ Bundle_rank_map.get(b));
	*/	sorted_bundle_list = Bundle_rank_map.entrySet().stream().sorted(Entry.comparingByValue())
				.map(e -> e.getKey()).collect(Collectors.toList());
/*		for (Bundle b : sorted_bundle_list)
			System.out.println("sorted bid" + b.getBundleID());*/
		return new ArrayList<>(sorted_bundle_list);
	}	

	public List<Bundle> assignBundles(List<Bundle> sortedBundles) {

		List<Bundle> assignedBundles = new ArrayList<Bundle>();
		List<Bundle> temp;
		for (int i = 0; i < sortedBundles.size(); ++i) {
			temp = new ArrayList<Bundle>();
			assignedBundles.add(sortedBundles.get(i));
			for (int j = i + 1; j < sortedBundles.size(); j++) {
				if (sortedBundles.get(j).getNurseID() == sortedBundles.get(i).getNurseID()) {
					temp.add(sortedBundles.get(j));
				}
				if (!Collections.disjoint(sortedBundles.get(j).getRequestIdList(),
						sortedBundles.get(i).getRequestIdList())) {
					temp.add(sortedBundles.get(j));
				}
			}
			temp = temp.stream().distinct().collect(Collectors.toList());
			sortedBundles.removeAll(temp);
		}
		return assignedBundles;
	}


	public List<Request> getBackupRequests(List <Bundle> assignedBundles) {
		RequestDAO requestService = new RequestDAO();
		Set<Integer> requestIdList = requestService.getAllStoredRequestId();
		for (Bundle b : assignedBundles) {
			requestIdList.removeAll(b.getRequestIdList());
		}
		return requestService.getRequestListById(requestIdList);
	}



}
